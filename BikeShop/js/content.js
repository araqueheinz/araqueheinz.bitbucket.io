/*
 * Use AJAX to load the JSON and manipulate the HTML
 * https://jbonline.bitbucket.io/data/bikeshop.json
*/

//Create XMLHttpRequest




/* /////////////////////////////////////////////////////////////////////// */
/* /                         PRODUCTS                                    / */
/* /////////////////////////////////////////////////////////////////////// */


if(document.getElementById('products')){

  //Create XMLHttpRequest
  var xhrProduct = new XMLHttpRequest();

  xhrProduct.onload = function()

  {
    //Create a variable to store JSON data
    var responseObject =JSON.parse(xhrProduct.responseText);

    //Build up string with new content
    var newProduct = '';

    //Personal check
    console.log("Its reading onlaod!");

    newProduct += '<ul>';
    for(var i = 0; i < 4; i++)
    {
      newProduct += '<li>';
      newProduct += '<p class="thumbnail"> <img src="' + responseObject.products[i].imageURL+'" alt="'+responseObject.products.title+'"></p>';
      newProduct += '<p data-rating="'+responseObject.products[i].rating+'"><meter min="0" max="5" value="'+responseObject.products[i].rating+'"></p>';
      newProduct += '<h3>'+ responseObject.products[i].title + '</h3>';
      newProduct += '<p>'+ responseObject.products[i].description.substring(0,32)+'...</p>';

      if(responseObject.products[i].salePrice > 0){
        newProduct += '<p>$'+responseObject.products[i].salePrice+'  <del>$'+responseObject.products[i].price+'</del></p>';
        newProduct += '<p class="sale">Sale</p>';
      }
      else{
        newProduct += '<p>$'+responseObject.products[i].price+'</p>';
      }

      newProduct += '<p><button>Add to Cart</button></p>';
      newProduct += '</li>';
    }

    newProduct+= '</ul>';
    //update the page with new content
    var content = document.querySelector('section#products h2');
    content.insertAdjacentHTML('afterEnd', newProduct);

  };

  //Prepare the request
  xhrProduct.open("GET", "https://jbonline.bitbucket.io/data/bikeshop.json", true);

  //Send the XMLHttpRequest
  xhrProduct.send(null);

}


/* /////////////////////////////////////////////////////////////////////// */
/* /                         CLUB EVENTS                                 / */
/* /////////////////////////////////////////////////////////////////////// */

if(document.getElementById('events')){

  var xhrEvent = new XMLHttpRequest();
  xhrEvent.onload = function()

  {
    //Create a variable to store JSON data
    var responseObject =JSON.parse(xhrEvent.responseText);

    //Build up string with new content
    var newEvent = '';

    //Personal check
    console.log("Its reading onlaod!");

    newEvent += '<ul>';
    for(var i = 0; i < 3; i++)
    {
      newEvent += '<li>';
      newEvent += '<img src="images/event-'+i+'.jpg" alt="'+ responseObject.events[i].title +'">';
      newEvent += '<h3>'+ responseObject.events[i].title + '</h3>';
      newEvent += '<p>'+ responseObject.events[i].text+'</p>';

      newEvent += '<dl>';
      newEvent += '<dt>by:</dt>';
      newEvent += '<dd>Full Sail University</dd>';
      newEvent += '<dt>Location:</dt>';
      newEvent += '<dd>'+ responseObject.events[i].location+'</dd>';
      newEvent += '<dt>Date:</dt>';
      newEvent += '<dd>'+ responseObject.events[i].date+'</dd>';
      newEvent += '</dl>'
      newEvent += '</li>';
    }

    newEvent+= '</ul>';
    //update the page with new content
    var content = document.querySelector('section#events h2');
    content.insertAdjacentHTML('afterEnd', newEvent);

  };

  //Prepare the request
  xhrEvent.open("GET", "https://jbonline.bitbucket.io/data/bikeshop.json", true);

  //Send the XMLHttpRequest
  xhrEvent.send(null);

}

/* /////////////////////////////////////////////////////////////////////// */
/* /                         BLOG POST                                   / */
/* /////////////////////////////////////////////////////////////////////// */

if(document.getElementById('posts')){

  var xhrPost = new XMLHttpRequest();
  xhrPost.onload = function()

  {
    //Create a variable to store JSON data
    var responseObject =JSON.parse(xhrPost.responseText);

    //Build up string with new content
    var newPost = '';

    //Personal check
    console.log("Its reading onlaod!");

    newPost += '<ul>';
    for(var i = 0; i < 3; i++)
    {
      newPost += '<li>';
      newPost += '<img src="' + responseObject.posts[i].imageURL+'" alt="'+responseObject.posts.title+'">';
      newPost += '<h3>'+ responseObject.posts[i].title + '</h3>';
      newPost += '<dl>';
      newPost += '<dt>Author:</dt>';
      newPost += '<dd>Full Sail University</dd>';
      newPost += '<dt>Date:</dt>';
      newPost += '<dd>'+ responseObject.posts[i].postDate + '</dd>';
      newPost += '<dt>Comments:</dt>';
      newPost += '<dd>'+ (i+1) + '</dd>';
      newPost += '</dl>';
      newPost += '<p>'+ responseObject.posts[i].text +'...</p>';
      newPost += '<p><button>Read More</button></p>';
      newPost += '</li>';
    }

    newPost+= '</ul>';
    //update the page with new content
    var content = document.querySelector('section#posts h2');
    content.insertAdjacentHTML('afterEnd', newPost);

  };

  //Prepare the request
  xhrPost.open("GET", "https://jbonline.bitbucket.io/data/bikeshop.json", true);

  //Send the XMLHttpRequest
  xhrPost.send(null);

}

/* /////////////////////////////////////////////////////////////////////// */
/* /                         CLUB BENEFITS                               / */
/* /////////////////////////////////////////////////////////////////////// */

if(document.getElementById('benefits')){

  var xhrBenefit = new XMLHttpRequest();
  xhrBenefit.onload = function()

  {
    //Create a variable to store JSON data
    var responseObject =JSON.parse(xhrBenefit.responseText);

    //Build up string with new content
    var newBenefit = '';

    //Personal check
    console.log("Its reading onlaod!");

    newBenefit += '<ul>';

      newBenefit += '<li>';
      newBenefit += '<img src="images/truck.png" alt="">';
      newBenefit += '<h3>'+ responseObject.benefits[0].title + '</h3>';
      newBenefit += '<p>'+ responseObject.benefits[0].description +'</p>';
      newBenefit += '</li>';

      newBenefit += '<li>';
      newBenefit += '<img src="images/wrench.png" alt="">';
      newBenefit += '<h3>'+ responseObject.benefits[1].title + '</h3>';
      newBenefit += '<p>'+ responseObject.benefits[1].description +'</p>';
      newBenefit += '</li>';

      newBenefit += '<li>';
      newBenefit += '<img src="images/envelope.png" alt="">';
      newBenefit += '<h3>'+ responseObject.benefits[2].title + '</h3>';
      newBenefit += '<p>'+ responseObject.benefits[2].description +'</p>';
      newBenefit += '<form action="index.html" method="post">';
      newBenefit += '<p><input id="email" type="email" name="email" placeholder="john@example.com" value=""></p>';
      newBenefit += '<p><input id="submit" type="submit" value="Sign Up"></p>'
      newBenefit += '</form>';
      newBenefit += '</li>';


    newBenefit+= '</ul>';
    //update the page with new content
    var content = document.querySelector('section#benefits h2');
    content.insertAdjacentHTML('afterEnd', newBenefit);

  };

  //Prepare the request
  xhrBenefit.open("GET", "https://jbonline.bitbucket.io/data/bikeshop.json", true);

  //Send the XMLHttpRequest
  xhrBenefit.send(null);

}
