/*
 * events.js
 *
*/

/* /////////////////////////////////////////////////////////////////////// */
/* /                        LOAD MORE PRODUCS EVENT                      / */
/* /////////////////////////////////////////////////////////////////////// */


if(document.getElementById('products')){
  document.querySelector('#more-products').addEventListener('click', loadProducts)
}

function loadProducts()
{

  if(document.getElementById('products'))
  {
    var xhrAddProduct = new XMLHttpRequest();
    xhrAddProduct.onload = function() {

      var responseObject =JSON.parse(xhrAddProduct.responseText);
      var addProduct = '';


      for(var i = 4; i < 8; i++)
      {
        addProduct += '<li>';
        addProduct += '<p class="thumbnail"> <img src="' + responseObject.products[i].imageURL+'" alt="'+responseObject.products.title+'"></p>';
        addProduct += '<p data-rating="'+responseObject.products[i].rating+'"><meter min="0" max="5" value="'+responseObject.products[i].rating+'"></p>';
        addProduct += '<h3>'+ responseObject.products[i].title + '</h3>';
        addProduct += '<p>'+ responseObject.products[i].description.substring(0,32)+'...</p>';

        if(responseObject.products[i].salePrice > 0){
          addProduct += '<p>$'+responseObject.products[i].salePrice+'  <del>$'+responseObject.products[i].price+'</del></p>';
          addProduct += '<p class="sale">Sale</p>';
        }
        else{
          addProduct += '<p>$'+responseObject.products[i].price+'</p>';
        }

        addProduct += '<p><button>Add to Cart</button></p>';
        addProduct += '</li>';
      }

      var content = document.querySelector('#products ul li:last-child');
      content.insertAdjacentHTML('afterEnd', addProduct);

    };


    xhrAddProduct.open("GET", "https://jbonline.bitbucket.io/data/bikeshop.json", true);

    xhrAddProduct.send(null);

    document.querySelector('#more-products').style.display = 'none';
  }

}

/* /////////////////////////////////////////////////////////////////////// */
/* /                        CHECK EMAIL                                  / */
/* /////////////////////////////////////////////////////////////////////// */




if(document.querySelector('#name')&&document.querySelector('#phone') &&document.querySelector('#email')){

  document.querySelector('#name').addEventListener('blur', minChar);
  document.querySelector('#phone').addEventListener('blur', minChar);

  document.querySelector('#email').addEventListener('blur', function(event){

    var target = event.target;
    var parent = target.parentElement;
    var targetString =  target.value.toString();

    if (target.value.length < 4) {
      alert("Must be at least 8");
    }
    else if(targetString.indexOf("@") === -1){
      alert("no @ sign found");
    }
    else if(targetString.indexOf(".com") === -1 && targetString.indexOf(".edu") === -1){
      alert("no .com or .edu found");
    }

  });

  function minChar(event)
  {
    var target = event.target;
    var parent = target.parentElement;
    if (target.value.length < 8) {
      alert("Must be at least 8 characters!");
    }
    else {

    }
  }


}
